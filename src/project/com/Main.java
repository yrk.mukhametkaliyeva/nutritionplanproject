package project.com;


import project.com.Builders.MaintenanceNutritionPlanBuilder;
import project.com.Builders.WeightGainNutritionPlanBuilder;
import project.com.Builders.WeightLossNutritionPlanBuilder;
import project.com.UserManagement.User;
import project.com.UserManagement.UserManagement;

public class Main {
    private static UserManagement userManagement;
    private static DietaryCenter dietaryCenter;

    public static void main(String[] args) {
        setUp();

        // наши пользователи
        User user = new User("John", 60.00, userManagement.generateID()); // id:0
        userManagement.addUser(user);
        User user1 = new User("Live", 70.90, userManagement.generateID());// id:1
        userManagement.addUser(user1);
        User user2 = new User("Alice", 80.10, userManagement.generateID());// id:2
        userManagement.addUser(user2);

        WeightGainNutritionPlanBuilder wgnpb = new WeightGainNutritionPlanBuilder();
        dietaryCenter.createDiary(wgnpb, user.getId());

        MaintenanceNutritionPlanBuilder mnpb = new MaintenanceNutritionPlanBuilder();
        dietaryCenter.createDiary(mnpb, user1.getId());

        WeightLossNutritionPlanBuilder wlpb = new WeightLossNutritionPlanBuilder();
        dietaryCenter.createDiary(wlpb, user2.getId());

        System.out.println(dietaryCenter.getDailyCaloricIntake(user1.getId()));
        System.out.println(dietaryCenter.getMealPlan(user2.getId()));
    }


    public static void setUp() {
        userManagement = new UserManagement();
        NutritionPlanDirector nutritionPlanDirector = new NutritionPlanDirector();
        dietaryCenter = new DietaryCenter(userManagement, nutritionPlanDirector);
    }
}
