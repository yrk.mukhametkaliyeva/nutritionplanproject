package project.com.Builders;

import project.com.Builders.NutritionPlanBuilder;
import project.com.NutritionPlan;

import java.util.ArrayList;
import java.util.List;

public class MaintenanceNutritionPlanBuilder extends NutritionPlanBuilder {
    private int dailyCaloricIntake;
    private int carb;
    private int protein;
    private int fat;
    private List<String> mealPlan;
    private String fitnessGoal;               //"weight loss", "weight gain", "maintenance"
    private List<String> dietaryRestrictions;

    @Override
    public void setCaloricIntake(int calorie) {
        this.dailyCaloricIntake = calorie;
    }

    @Override
    public void setMacronutrientRatios(int carb, int protein, int fat) {
        this.carb = carb;
        this.protein = protein;
        this.fat = fat;
    }

    @Override
    public void setMealPlans(List<String> plan) {
        this.mealPlan = plan;
    }

    @Override
    public void setFitnessGoal(String type) {
        this.fitnessGoal = type;
    }

    @Override
    public void setDietaryRestrictions(List<String> rest) {
        this.dietaryRestrictions = rest;
    }

    @Override
    public NutritionPlan build() {
        List<String> mealplan = new ArrayList<>();
        List<String> restrictions = new ArrayList<>();
        restrictions.add("lactose-free");
        mealplan.add("Breakfast:any dish");
        mealplan.add("Lunch:rice");
        mealplan.add("Dinner:salad");
        setFitnessGoal("Maintenance");
        setMealPlans(mealplan);
        setCaloricIntake(2800);
        setMacronutrientRatios(25, 13, 30);
        return new NutritionPlan(dailyCaloricIntake, carb, protein, fat, mealPlan, fitnessGoal, dietaryRestrictions);
    }
}
