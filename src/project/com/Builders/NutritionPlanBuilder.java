package project.com.Builders;

import project.com.NutritionPlan;

import java.util.*;

public class NutritionPlanBuilder {
    private static NutritionPlan np;

    private int dailyCaloricIntake;
    private int carb;
    private int protein;
    private int fat;
    private List<String> mealPlan;
    private String fitnessGoal;               //"weight loss", "weight gain", "maintenance"
    private List<String> dietaryRestrictions;

    public void setCaloricIntake(int calorie) {
        this.dailyCaloricIntake = calorie;
    }

    public void setMacronutrientRatios(int carb, int prot, int fat) {
        this.carb = carb;
        this.protein = prot;
        this.fat = fat;
    }

    public void setMealPlans(List<String> plan) {
        this.mealPlan = plan;
    }

    public void setFitnessGoal(String type) {
        this.fitnessGoal = type;
    }

    public void setDietaryRestrictions(List<String> restrictions) {
        this.dietaryRestrictions = restrictions;
    }

    public NutritionPlan build() {
        return np;
    }
}
