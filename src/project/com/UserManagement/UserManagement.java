package project.com.UserManagement;

import java.util.ArrayList;
import java.util.List;

public class UserManagement {

    private List<User> userList = new ArrayList<>();

    public User getUserByID(int id) {
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getId() == id) {
                return userList.get(i);
            }
        }
        return null;

    }


    public void addUser(User user) {
        userList.add(user);
    }

    public int generateID() {
        return userList.size();
    }
}
