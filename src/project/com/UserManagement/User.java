package project.com.UserManagement;

import project.com.NutritionPlan;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    private double weight;
    private int id;
    private List<NutritionPlan> nutritionPlans = new ArrayList<>();

    public User(String name, double weight, int id) {
        this.name = name;
        this.weight = weight;
        this.id = id;
    }

    public void userNutritionPlan(NutritionPlan nutritionPlan) {
        nutritionPlans.add(nutritionPlan);
    }

    public NutritionPlan getNutritionPlan(){
        return nutritionPlans.get(nutritionPlans.size()-1);
    }

    public int getId() {
        return id;
    }
}
