package project.com;


import project.com.Builders.NutritionPlanBuilder;

public class NutritionPlanDirector {
    private NutritionPlanBuilder np;

    public void setBuilder(NutritionPlanBuilder np) {
        this.np = np;
    }

    public NutritionPlan createNutritionPlan() {
        return np.build();
    }

}
