package project.com;

import project.com.Builders.NutritionPlanBuilder;

import java.util.List;

public interface DietaryCenterFacade {
    void createDiary(NutritionPlanBuilder nutritionPlanBuilder, int usersID);
    int getDailyCaloricIntake(int userId);
    List<String> getMealPlan(int userId);
}
