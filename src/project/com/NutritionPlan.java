package project.com;

import java.util.List;

public class NutritionPlan {
    private final int dailyCaloricIntake;
    private final int carb;
    private final int protein;
    private int fat;
    private List<String> mealPlan;
    private String fitnessGoal;               //"weight loss", "weight gain", "maintenance"
    private List<String> dietaryRestrictions;   //"gluten-free", "vegan", "lactose-free")

    public NutritionPlan(int dailyCaloricIntake, int carb, int protein, int fat, List<String> mealPlan, String fitnessGoal, List<String> dietaryRestrictions) {
        this.dailyCaloricIntake = dailyCaloricIntake;
        this.carb = carb;
        this.protein = protein;
        this.fat = fat;
        this.mealPlan = mealPlan;
        this.fitnessGoal = fitnessGoal;
        this.dietaryRestrictions = dietaryRestrictions;
    }

    public int getDailyCaloricIntake() {
        return this.dailyCaloricIntake;
    }

    public List<String> getMealPlan() {
        return mealPlan;
    }

}
