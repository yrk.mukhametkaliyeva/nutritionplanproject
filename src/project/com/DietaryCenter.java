package project.com;

import project.com.Builders.NutritionPlanBuilder;
import project.com.UserManagement.User;
import project.com.UserManagement.UserManagement;

import java.util.List;

public class DietaryCenter implements DietaryCenterFacade {
    private final UserManagement userManagement;
    private final NutritionPlanDirector nutritionPlanDirector;


    public DietaryCenter(UserManagement userManagement, NutritionPlanDirector nutritionPlanDirector) {
        this.userManagement = userManagement;
        this.nutritionPlanDirector = nutritionPlanDirector;
    }

    @Override
    public void createDiary(NutritionPlanBuilder nutritionPlanBuilder, int usersID) {
        nutritionPlanDirector.setBuilder(nutritionPlanBuilder); // этап Builder pattern
        NutritionPlan nutritionPlan = nutritionPlanDirector.createNutritionPlan();
        User user = userManagement.getUserByID(usersID);
        if (user != null) {
            user.userNutritionPlan(nutritionPlan);
        }
    }

    @Override
    public int getDailyCaloricIntake(int userId) {
        User user = userManagement.getUserByID(userId);
        if (user == null) {
            throw new IllegalArgumentException("User not found for ID: " + userId);
        }

        NutritionPlan nutritionPlan = user.getNutritionPlan();
        if (nutritionPlan == null) {
            throw new IllegalStateException("No nutrition plan found for user ID: " + userId);
        }

        return nutritionPlan.getDailyCaloricIntake();
    }

    @Override
    public List<String> getMealPlan(int userId) {
        User user = userManagement.getUserByID(userId);
        if (user == null) {
            throw new IllegalArgumentException("User not found for ID: " + userId);
        }

        NutritionPlan nutritionPlan = user.getNutritionPlan();
        if (nutritionPlan == null) {
            throw new IllegalStateException("No nutrition plan found for user ID: " + userId);
        }

        return nutritionPlan.getMealPlan();
    }


}
